export { initialise };

function overlay(){
    var id_;

    function ob(){
        var overlay = document.createElement('div');
        overlay.id = 'overlay';
        overlay.className = 'overlay';
        overlay.innerHTML = "<h3>Overlay</h3>";
        document.body.appendChild(overlay);

        /* lets think about what we want to do here, basically
         * setup the divs with the appropriate id's
         */
    }

    //property setting/getting
    ob.id = function(v) {
        if(! arguments.length )return id_;
        id_ = v;
        return ob;
    }

    ob.update = function( state ){
        console.log( state );
    }

    return ob;
}

function initialise(){
    var id_;
    var translateM;
    var cam_world = {x:0,y:0,z:0};
    var entities = [];

    var overlay_;

    var kb_map = {
        toggleOverlay:  'd',
        moveUp:         'ArrowUp',
        moveDown:       'ArrowDown',
        moveLeft:       'ArrowLeft',
        moveRight:      'ArrowRight'
    }

    var mouse = {
        down: false,
        screen: {x:0,y:0},
        world: {x:0,y:0},
        delta: {x:0,y:0}
    }


    function ob(){

    }

    //Public accessors
    ob.id = function(v) {
        if(! arguments.length )return id_;
        id_ = v;
        return ob;
    }

    ob.enableOverlay = function(){
        console.log('stub: enableOverlay' );
        //Create new overlay object and add it to the list of objects to
        //update.
        var layer = overlay();
        entities.push( layer.update );
        layer();
        return ob;
    }

    ob.disableOverlay = function(){
        console.log('stub: disableOverlay');
        return ob;
    }

    ob.enableKeyboard = function(){
        window.addEventListener("keydown", kb_Listener );
        return ob;
    }

    ob.disableKeyboard = function(){
        window.removeEventListener("keydown", kb_Listener );
        return ob;
    }
    ob.enableMouse = function(){
        window.addEventListener('mousedown', mouse_Down );
        window.addEventListener('mouseup', mouse_Up );
        window.addEventListener('mousemove', mouse_Move );
        return ob;
    }
    ob.disableMouse = function(){
        window.removeEventListener('mousedown', mouse_Down );
        window.removeEventListener('mouseup', mouse_Up );
        window.removeEventListener('mousemove', mouse_Move );
        return ob;
    }

    ob.enableTouch = function(){
        console.log('stub: enableTouch');
    }
    ob.disableTouch = function(){
        console.log('stub: disableTouch');
    }

    ob.assignKey = function( key, action ){
        console.log('stub: assignKey');
        return ob;
    }

    //private functions
    function kb_Listener( event ){
        switch( event.key ) {
            case kb_map.moveLeft:
                translateX(-1);
                break;
            case kb_map.moveRight:
                translateX(1);
                break;
            case kb_map.moveUp:
                translateY(1);
                break;
            case kb_map.moveDown:
                translateY(-1);
                break;
            case kb_map.toggleOverlay:
                enableOverlay();
                break;
        }
        update();
    }

    function mouse_Down( event ){
        if( event.button == 0 ){
            mouse.delta.x = 0;
            mouse.delta.y = 0;
            mouse.down = true;
        }
    }

    function mouse_Up( event ){
        if( event.button == 0 ) mouse.down = false;
    }

    function mouse_Move( event ){
        if(! mouse.down )return;

        mouse.delta.x += event.movementX;
        mouse.delta.y += event.movementY;
        console.log(`stub: mouse_Move ${mouse.delta.x},${mouse.delta.y}`);
        translateX( event.movementX );
        translateY( event.movementY );
        update();
    }

    function translateX( distance ){
        console.log(`stub: translateX(${distance})`);
    }

    function translateY( distance ){
        console.log(`stub: translateY(${distance})`);
    }

    function update(){
        //basically a re-draw function, but this should update the list of
        //objects rather than do the work itself.
        entities.forEach( func => {
            func( this );
        });
    }

    return ob;
}


