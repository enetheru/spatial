# spatial
This repo contains my initial tests in a spatial navigation website, It's just
a concept for now, but something that has been on my mind lately.

The idea is that the website can be navigated like a platformer, but without
the character jumping around, you simply control the camera and can interact
with the world around you with the mouse.
