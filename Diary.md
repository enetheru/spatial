Sat 15 Aug 2020 12:12:54
-----------------------------
An idea came to mind regarding the locked tracking movement constraints, and it
made me think that either the arrow keys have to be modal in their use, or that
a separate key needs to be used to move 'forward' whatever forward means. That
implies the user is facing a direction, and that direction can be changed.

Which makes me think that multiple keypresses need to be able to happen at the
same time, which doesnt appear to be the default key repeating behaviour.

I might need to handle key up, and keydown separately to enable diagonal
movement.

I'm having a little stuck issue with where to build in separations, I am posing
the question to myself whether the interaction between the world and the world
be defined separately, and who has primary control over that world.

I think If I build in the world as basic as possible, and create a second
object that refines that control it would be easier to change.

Mon 17 Aug 2020 11:55:46
-----------------------------
Looks like I am going to have to create my own style of smooth scrolling,
averaging the finger stroke inputs and interpolating the result.

That gives me a little bit of frustration that I cannot create such a thing
simply, but I wouldn't really expect much more and I can use the same knowledge
again and again on various projects I have going.

Mon 17 Aug 2020 17:33:52
-----------------------------
The difficulty I'm facing is one of scope, I want to loop through a list of
objects and run their update functions using the current state of the system.

How do I get the state information into the object, I guess perhaps the update
functions will have to be given customised packet data?

I can split it into various things like movement as compared to triggers and
other things. But that doesn't change much about the mechanics, I still need to
pass the information.

And its not the object I need to give to the list, its the update function.

Thu 20 Aug 2020 10:09:51
-----------------------------
Found out today that touch events are very different from click and drag. So
will have to get that working separately.
